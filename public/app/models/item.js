define([
  'use!underscore',
  'use!backbone'
], function(_, Backbone) {
  var Item = Backbone.Model.extend({

    urlRoot: 'api/items',

    idAttribute: '_id'

  });

  return Item;
});
