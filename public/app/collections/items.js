define([
  'jquery',
  'use!underscore',
  'use!backbone',
  'models/item'
], function($, _, Backbone, Item){
  var Items = Backbone.Collection.extend({

    model: Item,

    url: 'api/items/'

  });

  return Items;
});
